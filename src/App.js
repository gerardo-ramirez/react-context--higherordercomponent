import React from 'react';
import { Link } from 'react-router-dom';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">

        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <Link
          className="App-link"
          to='/user'
        >
          User
        </Link>
        <Link
          className="App-link"
          to='/post'
        >
          Posts
        </Link>
        <Link
          className="App-link"
          to='/nesting'
        >
          Nesting
        </Link>
      </header>
    </div>
  );
}

export default App;
