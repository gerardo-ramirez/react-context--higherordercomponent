import React, { Component } from 'react';
import WrapperConsumer from '../store';
import Nivel2 from './nivel2';

class Nesting extends Component {
    componentDidMount() {
        console.log(this.props)
    }
    render() {
        return (
            <div>
                <h3>Nivel 1</h3>
                <Nivel2 />

            </div>
        )
    }

}
export default WrapperConsumer(Nesting);
