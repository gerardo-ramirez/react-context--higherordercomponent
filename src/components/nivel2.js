import React, { Component } from 'react';
import WrapperConsumer from '../store';
import Nivel3 from './nivel3';

class Nivel2 extends Component {
    render() {
        return (
            <div style={{ background: 'grey' }}>
                <h3>Nivel 2</h3>
                <Nivel3 />
            </div>
        )
    }

}
export default WrapperConsumer(Nivel2);
