import React, { Component } from 'react';
import WrapperConsumer from '../store';
import Nivel4 from './nivel4';
class Nivel3 extends Component {
    render() {
        return (
            <div style={{ background: 'blue' }}>
                <h3>Nivel 3</h3>
                <Nivel4 />

            </div>
        )
    }

}
export default WrapperConsumer(Nivel3);
