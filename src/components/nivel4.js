import React, { Component } from 'react';
import WrapperConsumer from '../store';
import Nivel5 from './nivel5';

class Nivel4 extends Component {
    render() {
        return (
            <div style={{ background: 'gold' }}>
                <h3>Nivel 4</h3>
                <Nivel5 />

            </div>
        )
    }

}
export default WrapperConsumer(Nivel4);
