import React from 'react';
import WrapperConsumer from '../store';


const Nivel5 = ({ context: { user, posts } }) => {


    return (
        <div style={{ background: 'lightblue' }}>
            <h3>Nivel 5</h3>
            <strong>{user.name}</strong>
            <h4>Posts</h4>
            <ul>
                {posts.map(post => {
                    return (
                        <li key={post.id}>{post.title}</li>
                    )
                })}
            </ul>

        </div>
    )


}
export default WrapperConsumer(Nivel5);
