import React, { Component } from 'react';
import WrapperConsumer from '../store';//este va a envolver todo en el Consumer.

class Post extends Component {
    componentDidMount() {
        console.log(this.props)
    }
    render() {
        //aplicando destructurin le pasamos el mismo nombre . 
        const { context: { posts } } = this.props;
        return (
            <div>
                <h3>Page Post</h3>
                {//agarramos cada elemento del array
                }
                {
                    posts.map(post => {
                        return (<p key={post.id}>{post.title}</p>)
                    }

                    )
                }
                < p > {posts.title}</p>
            </div>

        )
    }
}
export default WrapperConsumer(Post);