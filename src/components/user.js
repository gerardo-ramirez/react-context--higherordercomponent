import React, { Component } from 'react';
import WrapperConsumer from '../store';

class User extends Component {
    //obtenemos el store :
    componentDidMount() {
        console.log(this.props)
    }
    //para evitar renderizado de todos los componentes hacemos la siguiente comparacion:
    //comparamos propiedades.
    shouldComponentUpdate(prevProps, newProps) {
        return true;
    }
    /*
    */
    render() {
        const { context: { user, changeValue } } = this.props;
        return (
            <div>
                <p> {user.name}</p>
                <button onClick={changeValue}>Cambiar</button>
            </div>




        )
    }
}
export default WrapperConsumer(User);
/*
  {context => <Component {...props} context={context} />}
  component en este caso es user .
  se crea la constante con nombre del contexto (es decir value, estado )
  y pasamos las props . incluido metodo

*/