import React from 'react';
import App from '../App';
import User from '../components/user';
import Post from '../components/post';
import Nesting from '../components/nesting';


import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { ContextStore } from '../store';


const Routes = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path='/' render={props => <ContextStore comp={<App />} />} />
                {/* cramo una propiedad store donde le pasamos el estado de un solo elemento clave
                para que al llamarlo no nos traiga todos las claves */}
                <Route exact path='/user' render={props => <ContextStore comp={<User />} store='user' />} />
                <Route exact path='/post' render={props => <ContextStore comp={<Post />} />} />
                <Route exact path='/nesting' render={props => <ContextStore comp={<Nesting />} />} />




            </Switch>

        </BrowserRouter>

    )

}
export default Routes;
