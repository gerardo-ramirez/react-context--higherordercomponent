import React, { Component, createContext } from 'react';
const { Provider, Consumer } = createContext();

class ContextStore extends Component {
    changeValue = () => {
        this.setState({
            user: { name: 'Carlitos' }
        })
    };

    state = {
        user: { name: 'juan' },
        changeValue: this.changeValue,
        posts: [
            { id: 1, title: 'post 1' },
            { id: 2, title: 'post 2' },
            { id: 3, title: 'post 3' },
            { id: 4, title: 'post 4' },
            { id: 5, title: 'post 5' }
        ]
    }
    render() {
        const { store } = this.props
        return (
            /* si existe la propiedad store traeme el estado que tenga ese nombre
            si no traeme todo el estado
            (la propiedad store se la pase a User en routes) */
            <Provider value={store ? { [store]: this.state[store] } : this.state}>
                {this.props.comp}

            </Provider>
        )
    }
}

//CREAMOS UN HIGHERORDERCOMPONENT para poder utilizar el consumer desde el componentdidMout
const WrapperConsumer = (Component) => {
    //retornamos el mismo componente con nuevas props 
    return (props) => {
        return (
            //usamos el consumer 
            <Consumer>
                {//al componente le pasamos todas las props que pueda llegar a tener 
                    //mas context(es el contexto , puede tener cuaalquier nombre) pasado como props.

                    //contex es el value es decir el estado.
                }
                {context => <Component {...props} context={context} />}
                {/*se renderizo componente (cualquiera que este entre parentesis) con el estado y respetamos
                las props que ellos tenian ya */}
            </Consumer>
        )
    }
}













export default WrapperConsumer;
export { ContextStore };

